# 3088 Semester Project 

Repository created for the purposes of an exercise but may be used as the actual functional repo of the project depending on group members

This text added on master branch locally:
This goal of this project is to design a (micro) Hat for a raspberry pi zero which will act as an uninterruptible power supply. 
Such a device has many use cases and greatly expands the number of situations where the pi zero can be used. This is especially true for internet of things (IoT) applications in countries like south africa 
where a realiable source of mains power cannot be guarenteed. 

This text added in dev branch locally:
The goal of the project is to produce the hat in such a way as to require little to no management from the host pi. The idea is that the 
user loads the (very very minimal) programme to the pi which manages the shutdown procedure in the situation where the battery is depleted, then connects the board to the pi and uses it for whatever application they wish
 
